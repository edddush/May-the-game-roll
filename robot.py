import wpilib
import wpilib.drive
import ctre
class MyRobot(wpilib.IterativeRobot):
    
    def robotInit(self):
        self.ML0=ctre.wpi_talonsrx.WPI_TalonSRX(4)
        self.ML1=ctre.wpi_talonsrx.WPI_TalonSRX(3)
        self.ML0.setInverted(True)
        self.ML1.setInverted(True)
        self.left= wpilib.SpeedControllerGroup(self.ML0,self.ML1)

        self.MR1= ctre.wpi_talonsrx.WPI_TalonSRX(2)
        self.MR2= ctre.wpi_talonsrx.WPI_TalonSRX(1)
        self.right= wpilib.SpeedControllerGroup(self.MR1,self.MR2)

        self.drive= wpilib.drive.DifferentialDrive(self.left,self.right)

        self.stick= wpilib.Joystick(0)
        self.timer= wpilib.Timer()

       
    def autonomousInit(self):
        pass
    def autonomousPeriodic(self):
        pass
    def disabledInit(self):
        pass
    def disabledPeriodic(self):
        pass
    def teleopInit(self):
        pass 
    def teleopPeriodic(self):
        self.drive.arcadeDrive(-1*self.stick.getRawAxis(0), self.stick.getRawAxis(1))

if __name__=='__main__' :
    wpilib.run(MyRobot)
